import CurrencyEnum from "./CurrensyEnum";
import Transaction from "./Transaction";
import { ICard } from "./ICard";

class Card implements ICard {
    transactions: Transaction[] = [];

    addTransaction(transaction: Transaction): string;

    addTransaction(currency: CurrencyEnum, amount: number): string;

    addTransaction(arg1: Transaction | CurrencyEnum, amount?: number): string {
        if(arg1 instanceof Transaction) {
            this.transactions.push(arg1);
            console.log(`Transaction #${arg1.id} has been added`)
            return arg1.id;
        } else if(arg1 !== undefined && amount !== undefined) {
            const transaction = new Transaction(amount, arg1);
            this.transactions.push(transaction);
            console.log(`Transaction #${transaction.id} has been added`)
            return transaction.id;
        } else {
            throw new Error('Invalid parameters for Add Transaction');
        }
    }

    getTransaction(id: string): Transaction | undefined {
        return this.transactions.find(transaction => transaction.id === id);
    }

    getBalance(currency: CurrencyEnum): number {
        return this.transactions
            .filter(transaction => transaction.currency === currency)
            .reduce((total, transaction) => total + transaction.amount, 0);
    }
}

export default Card;
