import CurrencyEnum from "./CurrensyEnum";
import { ICard } from "./ICard";


class Pocket {
    private cards: Map<string, ICard> = new Map();

    addCard(name: string, card: ICard): void {
        this.cards.set(name, card);
        console.log(`${name} has beed added`)
    };

    getCard(name: string): ICard | undefined {
        console.log(this.cards.get(name));
        return this.cards.get(name);
    };
    
    removeCard(name: string): void {
        this.cards.delete(name);
        console.log(`${name} has beed deleted`)
    };

    getTotalAmount(currency: CurrencyEnum): number {
        let totalAmount = 0;
        for (const card of this.cards.values()) {
            totalAmount += card.getBalance(currency);
        };
        console.log(`In your pocket contains ${totalAmount} ${currency}`)
        return totalAmount;
    };
};

export default Pocket;
