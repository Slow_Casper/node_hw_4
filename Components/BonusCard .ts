import CurrencyEnum from "./CurrensyEnum";
import Transaction from "./Transaction";
import { ICard } from "./ICard";

class BonusCard implements ICard {
    transactions: Transaction[] = [];

    addTransaction(transaction: Transaction): string;

    addTransaction(currency: CurrencyEnum, amount: number): string;

    addTransaction(arg1: Transaction | CurrencyEnum, amount?: number): string {
        if(arg1 instanceof Transaction) {
            this.transactions.push(arg1);

            const bonusAmount = arg1.amount * 0.1;
            const bonusCurrenccy = arg1.currency;
            const bonusTransaction = new Transaction(bonusAmount, bonusCurrenccy);

            this.transactions.push(bonusTransaction);

            return (
                arg1.id,
                bonusTransaction.id
            );
        } else if(arg1 !== undefined && amount !== undefined) {
            const transaction = new Transaction(amount, arg1);
            this.transactions.push(transaction);

            const bonusAmount = amount * 0.1;
            const bonusCurrenccy = arg1;
            const bonusTransaction = new Transaction(bonusAmount, bonusCurrenccy);

            this.transactions.push(bonusTransaction);

            return transaction.id;
        } else {
            throw new Error('Invalid parameters for Add Transaction');
        }
    }

    getTransaction(id: string): Transaction | undefined {
        return this.transactions.find(transaction => transaction.id === id);
    }

    getBalance(currency: CurrencyEnum): number {
        return this.transactions
            .filter(transaction => transaction.currency === currency)
            .reduce((total, transaction) => total + transaction.amount, 0);
    }
}

export default BonusCard;
