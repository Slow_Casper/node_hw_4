import CurrencyEnum from "./CurrensyEnum";
import Transaction from "./Transaction";

export interface ICard {
    addTransaction(transaction: Transaction): string;
    addTransaction(currency: CurrencyEnum, amount: number): string;
    getTransaction(id: string): Transaction | undefined;
    getBalance(currency: CurrencyEnum): number
};
