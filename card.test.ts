import Card from "./Components/Card";
import BonusCard from "./Components/BonusCard ";
import CurrencyEnum from "./Components/CurrensyEnum";
import Pocket from "./Components/Pocket";

const cardsNames = {
    simple: 'Simple card',
    bonus: 'Bonus card',
};

const { simple, bonus } = cardsNames;

const card = new Card();
const bonusCard = new BonusCard();


// Додаємо до карток транзакції

card.addTransaction(CurrencyEnum.UAH, 200);
card.addTransaction(CurrencyEnum.USD, 200);
bonusCard.addTransaction(CurrencyEnum.USD, 500);
bonusCard.addTransaction(CurrencyEnum.USD, 100);


// Створюємо зразок Pocket

const pocket = new Pocket();


// Додоємо до гаманця картки

pocket.addCard(simple, card);
pocket.addCard(bonus, bonusCard);


// Отримуємо дані певної картки

pocket.getCard(simple);
pocket.getCard(bonus);


// Видалити певну картко 

pocket.removeCard(simple);


// Витягуємо кількість певної валюти з гаманця

pocket.getTotalAmount(CurrencyEnum.UAH);
pocket.getTotalAmount(CurrencyEnum.USD);



